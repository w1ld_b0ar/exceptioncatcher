package com.ablashchuk.exceptioncatcherlib;

import android.app.Application;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ExceptionCatcher {
    private static final String DEVICE = "DEVICE";
    private static final String ANDROID_VERSION = "ANDROID_VERSION";
    private static final String THREAD = "THREAD";
    private static final String EXCEPTION = "EXCEPTION";
    private static final String STACK_TRACE = "STACK_TRACE";
    private static final String USER = "USER";
    private static final String LOG_TAG = ExceptionCatcher.class.getCanonicalName();
    private static final String LOGS = "LOGS";
    private static final String EXCEPTION_LOG_DATA_FOLDER = "exception_log_data";
    private static ExceptionCatcher instance = new ExceptionCatcher();
    private UncaughtExceptionHandler uncaughtExceptionHandler;
    /**
     * Data that is common for all occuring exceptions in this session. May be updated in runtime,
     * i.e. user may log out and log in with other account - if {@link #setUser(String)} will be
     * called, these extras will again represent data, relevant to anything that will be logged after
     */
    private Map<String, String> commonExtraData = new HashMap<>();
    private Application application;
    private static final String UPLOAD_URL = "http://some_url_that_accepts_upload.com/";

    public static ExceptionCatcher getInstance() {
        return instance;
    }

    /**
     * Initialization of {@link ExceptionCatcher}. Should be called inside
     * {@link Application#onCreate()} and only once per application creation.
     *
     * @param application Application that wants to use exception logging
     * @return <code>true</code> if initialization was not called before. Opposite result may
     * highlight issue with calls to this method.
     */
    public static boolean init(Application application) {
        if (instance.uncaughtExceptionHandler != null) {
            return false;
        }
        instance.application = application;
        UncaughtExceptionHandler currentDefaultUncaughtExceptionHandler
                = Thread.getDefaultUncaughtExceptionHandler();
        if (currentDefaultUncaughtExceptionHandler != null) {
            instance.uncaughtExceptionHandler = new LoggingUncaughtExceptionHandlerDecorator(
                    currentDefaultUncaughtExceptionHandler);
        } else {
            instance.uncaughtExceptionHandler = new LoggingUncaughtExceptionHandler();
        }
        instance.setDevice(String.format("%1s(%2s %3s)", android.os.Build.MODEL,
                android.os.Build.MANUFACTURER, android.os.Build.PRODUCT));
        instance.setAndroidVersion(String.valueOf(android.os.Build.VERSION.SDK_INT));
        Thread.setDefaultUncaughtExceptionHandler(instance.uncaughtExceptionHandler);
        ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1,
                new ThreadFactory() {
            public Thread newThread(Runnable r) {
                Thread t = Executors.defaultThreadFactory().newThread(r);
                t.setDaemon(true); //shouldn't prevent app from termination once we are in background
                return t;
            }
        });
        executor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                instance.checkForUnsentFilesAndSend();
            }
        }, 0, 10, TimeUnit.MINUTES);
        return true;
    }

    private void checkForUnsentFilesAndSend() {
        File storedLogDataFilesFolder = new File(getStoredLogDataFilesFolderAbsolutePath());
        final File[] files = storedLogDataFilesFolder.listFiles();
        if (files.length != 0) {
            sendLogDataFiles(files);
        }
    }

    private void sendLogDataFiles(final File[] files) {
        new Thread() {
            @Override
            public void run() {
                for (int i = 0; i < files.length; i++) {
                    sendLogDataFile(files[i]);
                }
            }
        }.start();
    }

    private static String collectStackTrace(@Nullable Throwable throwable) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        Throwable cause = throwable;
        while (cause != null) {
            cause.printStackTrace(printWriter);
            cause = cause.getCause();
        }
        final String stacktraceString = result.toString();
        printWriter.close();
        return stacktraceString;
    }

    private void sendLogDataFile(final File file) {
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", file.getName(),
                        RequestBody.create(MediaType.parse("text/plain"), file))
                .addFormDataPart("app-id", application.getPackageName())
                .build();
        Request request = new Request.Builder()
                .url(UPLOAD_URL)
                .post(requestBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(LOG_TAG, "Failure during upload of file " + file.getName());
                /* no need to retry here if we have extra extra entrypoints for retry(every 10 mins
                and upon startup */
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    Log.e(LOG_TAG, "Unsuccessful upload of file " + file.getName());
                    Log.e(LOG_TAG, "Response is " + response.toString());
                    /* no need to retry here if we have extra extra entrypoints for retry(every 10
                    mins and upon startup */
                } else {
                    boolean fileDeleteSuccessful = file.delete();
                    if (!fileDeleteSuccessful) {
                        Log.w(LOG_TAG, "File " + file.getName() + " was not deleted successfully");
                    }
                }
            }
        });
    }

    private static String collectLogs() {
        StringBuilder logBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            Process process = Runtime.getRuntime().exec("logcat -d");
            bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                logBuilder.append(line).append("\n");
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Couldn't collect logs", e);
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Couldn't close buffered reader for logs collection", e);
                }
            }
        }
        return logBuilder.toString();
    }

    /**
     * Log non-fatal throwable. Relevant data will be automatically collected and added to report.
     */
    public void logException(Throwable throwable) {
        logException(throwable, Thread.currentThread(), false);
    }

    /**
     * Log throwable, either fatal or not.
     *
     * @param throwable Throwable to log. Stack trace, message and any other relevant data will be obtained from here
     * @param thread    Thread exception occured in
     * @param isFatal   indicator whether it is caught exception and application may proceed working
     *                  or it is uncaught and termination is unavoidable
     */
    private void logException(Throwable throwable, Thread thread, boolean isFatal) {
        if (uncaughtExceptionHandler == null) {
            throw new IllegalStateException("ExceptionCatcher was never initialized!");
        }
        Map<String, String> extraToSerialize = new HashMap<>(instance.commonExtraData);
        extraToSerialize.put(THREAD, thread.toString());
        extraToSerialize.put(EXCEPTION, throwable.toString());
        extraToSerialize.put(STACK_TRACE, collectStackTrace(throwable));
        extraToSerialize.put(LOGS, collectLogs());
        JSONObject object = new JSONObject(extraToSerialize);
        FileWriter fileWriter = null;
        try {
            String folderName = getStoredLogDataFilesFolderAbsolutePath();
            String fileName = folderName + "/"
                    + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSSS'.txt'").format(new Date());
            File folder = new File(folderName);
            folder.mkdirs();
            fileWriter = new FileWriter(fileName);
            fileWriter.write(object.toString());
        } catch (IOException e) {
            Log.e(LOG_TAG, "Can't finish logging exception", e);
        } finally {
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Can't finish saving logged exception", e);
                }
            }
        }
        if (isFatal) {
            //some kind of restart could have been here. or saying "sorry" to user :)
        }
    }

    @NonNull
    private String getStoredLogDataFilesFolderAbsolutePath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + EXCEPTION_LOG_DATA_FOLDER;
    }

    /**
     * Add string-representable info about user. Name, e-mail, phone, ID or whatever will help
     * identify user and solve problem with thrown exception in case of one
     *
     * @param user String representation of data to be saved
     */
    public void setUser(String user) {
        commonExtraData.put(USER, user);
    }

    /**
     * Add string-representable info of any kind. This may be any field names-values, called
     * methods, created important objects, sizes of arrays - whatever that can help debugging and
     * can be represented as String.
     *
     * @param key   key under which this data is to be attached to exception log
     * @param value value to store with this key
     */
    public void addExtra(String key, String value) {
        commonExtraData.put(key, value);
    }

    private void setDevice(String device) {
        commonExtraData.put(DEVICE, device);
    }

    private void setAndroidVersion(String androidVersion) {
        commonExtraData.put(ANDROID_VERSION, androidVersion);
    }

    private static class LoggingUncaughtExceptionHandler implements UncaughtExceptionHandler {

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            instance.logException(e, t, true);
        }
    }

    private static class LoggingUncaughtExceptionHandlerDecorator
            extends LoggingUncaughtExceptionHandler {

        private UncaughtExceptionHandler decoratedHandler;

        public LoggingUncaughtExceptionHandlerDecorator(UncaughtExceptionHandler decoratedHandler) {
            this.decoratedHandler = decoratedHandler;
        }

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            super.uncaughtException(t, e);
            if (decoratedHandler != null) {
                decoratedHandler.uncaughtException(t, e);
            }
        }

        public void setDecoratedHandler(UncaughtExceptionHandler decoratedHandler) {
            this.decoratedHandler = decoratedHandler;
        }
    }
}
