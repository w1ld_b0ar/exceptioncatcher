package com.ablashchuk.exceptioncatcher;

import android.app.Application;
import com.ablashchuk.exceptioncatcherlib.ExceptionCatcher;


public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ExceptionCatcher instance = ExceptionCatcher.getInstance();
        ExceptionCatcher.init(this);
        instance.setUser("username@domain.com");//in case there is some kind of logging in and related info
    }
}
