package com.ablashchuk.exceptioncatcher;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.ablashchuk.exceptioncatcherlib.ExceptionCatcher;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private int buttonOneClicksCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onCaughtException(View view) {
        ExceptionCatcher.getInstance().addExtra("BUTTON_ONE_CLICKS_AMOUNT",
                String.valueOf(++buttonOneClicksCounter));
        Exception exception = new IOException("Cannot read configuration",
                new FileNotFoundException("No file found under /data/data/1234"));
        ExceptionCatcher.getInstance().logException(exception);
    }


    public void onUncaughtException(View view) {
        ExceptionCatcher.getInstance().addExtra("LAST_CLICKED_BUTTON", "button-2");
        throw new RuntimeException("WASTED");
    }
}
